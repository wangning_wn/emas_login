Pod::Spec.new do |s|

  s.name         = "Login"
  s.version      = "1.0.0"
  s.summary      = "Login类库"

  s.description  = <<-DESC
                   * Detail about Login framework.
                   DESC

  s.homepage     = "http://"
  s.license      = 'MIT (example)'
  s.author       = { "未定义" => "undefined" }
  s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'
  s.source       = { :http => "http://Login.zip" }
  s.preserve_paths = "Login.framework/*"
  s.resources  = "Login.framework/*.{bundle,xcassets}"
  s.vendored_frameworks = 'Login.framework'
  s.requires_arc = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Login' }

  #s.dependency 'XXXX'

end
